# Ring oscillator with RP2040 PIO

This is a demonstration of the RP2040's PIO system, which let you run several state machines independently of the CPU. There's exactly 0 overhead and the state machines run a custom assembly language with simplified instructions.

In this example implementation of a [ring oscillator](https://pub.pages.cba.mit.edu/ring/), pin 1 is configured as a PIO input, and pin 2 as an output. Reading, inverting and writing is as simple as:

```
mov pins, !pins
```

Where `pins` is a meta keyword, with a different meaning in the first and second operand (output VS input, respectively).

## Results

| CPU Freq | Ring Freq  |
| -------- | ---------- |
| 133 MHz  | 16.667 MHz |
| 250 MHz  | 25 MHz     |

## C implementation

The original C implementation can be found [here](./c).

## MicroPython implementation

A MicroPython version with the same performance can be found [here](./py).
