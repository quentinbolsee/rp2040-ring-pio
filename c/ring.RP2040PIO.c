//
// ring.RP2040PIO.c
//    RP2040 ring oscillator test with PIO
//    connect P1 and P2
//
// Quentin Bolsee 12/27/22
//
// This work may be reproduced, modified, distributed,
// performed, and displayed for any purpose, but must
// acknowledge this project. Copyright is retained and
// must be preserved. The work is provided as is; no
// warranty is provided, and users accept all liability.
//

#include "pico/stdlib.h"
#include "hardware/pio.h"
#include "hardware/clocks.h"
#include "hardware/structs/sio.h"
#include "ring.RP2040PIO.pio.h"

#define IN_PIN  1
#define OUT_PIN 2

int main() {
    // set_sys_clock_khz(133000, false);
    set_sys_clock_khz(250000, false);

    PIO pio = pio0;
    uint offset = pio_add_program(pio, &ring_program);
    uint sm = pio_claim_unused_sm(pio, true);
    ring_program_init(pio, sm, offset, IN_PIN, OUT_PIN);
}
