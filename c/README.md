# Ring oscillator with RP2040 PIO, C implementation

This is a demonstration of the RP2040's PIO system, which let you run several state machines independently of the CPU. There's exactly 0 overhead and the state machines run a custom assembly language with simplified instructions.

## Binaries

You can find compiled binaries in [build/](./build/). Simply drang and drop the `.uf2` file on your raspberry pi pico's storage space after putting it in reset mode (boot button + power cycle).

## Build

Navigate to `build/` and generate the project using cmake:

```
cd build/
cmake ..
```

This should create a Makefile, and you can simply build the project every time you update the code by running:

```
make
```

The output is `bin`, `elf`, `hex` and `uf2` files in `build/`.
