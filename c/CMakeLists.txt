cmake_minimum_required(VERSION 3.13)

include($ENV{PICO_SDK_PATH}/external/pico_sdk_import.cmake)

project(ring C CXX ASM)

set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 17)

pico_sdk_init()

add_executable(ring)

pico_generate_pio_header(ring ${CMAKE_CURRENT_LIST_DIR}/ring.RP2040PIO.pio)

target_sources(ring PRIVATE ring.RP2040PIO.c)

target_link_libraries(ring pico_stdlib hardware_pio)

pico_add_extra_outputs(ring)
