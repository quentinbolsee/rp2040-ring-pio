# Ring oscillator with RP2040 PIO, MicroPython implementation

Here is a MicroPython implementation of using the RP2040's PIO system for a [ring oscillator](https://pub.pages.cba.mit.edu/ring/). The PIO asm is a one liner:

```
mov pins, !pins
```

The rest of the code simply sets up the state machine and optionally overclocks the RP2040 to 250MHz.

## Installing MicroPython

You can install MicroPython from [the official website](https://micropython.org/download/), and a good setup guide can be found [here](https://wiki.seeedstudio.com/XIAO-RP2040-with-MicroPython/).

## Code

The code can be found [here](./ring.RP2040PIO.py), but it's short enough to paste here:

```py
#
# ring.RP2040PIO.py
#    RP2040 ring oscillator with PIO
#    connect P1 and P2
#
# Quentin Bolsee 12/27/22
#
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose, but must
# acknowledge this project. Copyright is retained and
# must be preserved. The work is provided as is; no
# warranty is provided, and users accept all liability.
#

from machine import Pin, freq
import rp2


@rp2.asm_pio(out_init=rp2.PIO.OUT_LOW)
def ring():
    mov(pins, invert(pins))


# clock speed
# freq(133000000)
freq(250000000)
pin_in = Pin(1, Pin.IN)
pin_out = Pin(2, Pin.OUT)
sm = rp2.StateMachine(0, ring, in_base=pin_in, out_base=pin_out)
sm.active(1)
```
