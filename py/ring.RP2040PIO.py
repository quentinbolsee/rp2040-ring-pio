#
# ring.RP2040PIO.py
#    RP2040 ring oscillator with PIO
#    connect P1 and P2
#
# Quentin Bolsee 12/27/22
#
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose, but must
# acknowledge this project. Copyright is retained and
# must be preserved. The work is provided as is; no
# warranty is provided, and users accept all liability.
#

from machine import Pin, freq
import rp2


@rp2.asm_pio(out_init=rp2.PIO.OUT_LOW)
def ring():
    mov(pins, invert(pins))


# clock speed
# freq(133000000)
freq(250000000)
pin_in = Pin(1, Pin.IN)
pin_out = Pin(2, Pin.OUT)
sm = rp2.StateMachine(0, ring, in_base=pin_in, out_base=pin_out)
sm.active(1)
