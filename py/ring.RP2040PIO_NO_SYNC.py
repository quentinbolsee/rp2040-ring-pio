#
# ring.RP2040PIO_NO_SYNC.py
#    RP2040 ring oscillator with PIO + bypass synchronizer
#    connect P1 and P2
#
# Quentin Bolsee 2024-03-25
#
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose, but must
# acknowledge this project. Copyright is retained and
# must be preserved. The work is provided as is; no
# warranty is provided, and users accept all liability.
#

from machine import Pin, freq, mem32
import rp2


@rp2.asm_pio(out_init=rp2.PIO.OUT_LOW)
def ring():
    mov(pins, invert(pins))


# clock speed
# freq(133000000)
freq(250000000)

# IO pins
pin_in = Pin(1, Pin.IN)
pin_out = Pin(2, Pin.OUT)

# bypass PIO input synchronizer on pin 1
PIO0 = const(0x50200000)
INPUT_SYNC_BYPASS = const(0x038)
mem32[PIO0 + INPUT_SYNC_BYPASS] |= 1 << 1

# create and launch state machine
sm = rp2.StateMachine(0, ring, in_base=pin_in, out_base=pin_out)
sm.active(1)
